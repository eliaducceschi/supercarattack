#pragma once

#include "Kismet/BlueprintFunctionLibrary.h"
#include "SuperCarAttackFunctionLibrary.generated.h"

UCLASS()
class SUPERCARATTACK_API USuperCarAttackFunctionLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
	
public:

	static UProperty* RetrieveProperty(UObject *From, FString PropertyPath, UObject *&OutTargetObject);
	
};
