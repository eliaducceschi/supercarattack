#pragma once

#include "GameFramework/Actor.h"
#include "Array.h"
#include "RaceManager.generated.h"

UCLASS(Blueprintable, BlueprintType)
class SUPERCARATTACK_API ARaceManager : public AActor
{
	GENERATED_BODY()

public:

	UPROPERTY(Category = "Components", VisibleAnywhere, BlueprintReadOnly)
	class UBillboardComponent *Billboard;

	UPROPERTY(Category = "Track", EditAnywhere, BlueprintReadWrite)
	TArray<class ACheckpoint*> Checkpoints;

	UPROPERTY(Category = "Track", VisibleAnywhere, BlueprintReadOnly)
	int NumCheckpoints;

private:

	class ASuperPlayerController *PlayerController;

public:	

	ARaceManager();

	UFUNCTION(BlueprintCallable)
	void LapCheck(int CheckpointPassedIndex);

protected:

	virtual void BeginPlay() override;

private:

	void StartSequence();
	
};
