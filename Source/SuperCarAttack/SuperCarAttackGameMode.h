// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/GameModeBase.h"
#include "SuperCarAttackGameMode.generated.h"

UCLASS(minimalapi)
class ASuperCarAttackGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ASuperCarAttackGameMode();
};



