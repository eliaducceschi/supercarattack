// Fill out your copyright notice in the Description page of Project Settings.

#include "SuperCarAttack.h"
#include "SuperCarAttackFunctionLibrary.h"

UProperty* USuperCarAttackFunctionLibrary::RetrieveProperty(UObject *From, FString PropertyPath, UObject *&OutTargetObject)
{
	if (From == nullptr)
	{
		return nullptr;
	}

	TArray<FString> InPath;
	if (!PropertyPath.ParseIntoArray(InPath, TEXT("."), true))
	{
		return nullptr;
	}

	OutTargetObject = nullptr;

	UClass *Class = From->GetClass();
	AActor* Actor = Cast<AActor>(From);

	if (Actor != nullptr && InPath.Num() > 1)
	{
		for (auto Component : Actor->GetComponents())
		{
			FString ComponentName = Component->GetName();
			FString TargetName = InPath[0];

			if (TargetName.Equals(ComponentName))
			{
				Class = Component->GetClass();
				OutTargetObject = Component;

				InPath.RemoveAt(0);
				break;
			}
		}
	}

	UProperty *ToRetrieve = nullptr;

	if (Class != nullptr && InPath.Num() > 0)
	{
		ToRetrieve = FindField<UProperty>(Class, *(InPath[0]));
	}

	return ToRetrieve;

}


