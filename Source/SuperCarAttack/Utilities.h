#pragma once

#include "Kismet/BlueprintFunctionLibrary.h"
#include "Utilities.generated.h"

UCLASS()
class SUPERCARATTACK_API UUtilities : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
	
public:

	UFUNCTION(BlueprintCallable)
	static FText TimeToText(float Time);	

private:

	static FString ZeroFormat(int ToFormat);
	
};
