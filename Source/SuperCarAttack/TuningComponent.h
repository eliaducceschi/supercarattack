#pragma once

#include "Components/ActorComponent.h"
#include "TuningComponent.generated.h"

USTRUCT(Blueprintable, BlueprintType)
struct SUPERCARATTACK_API FTuningModifier
{
	GENERATED_USTRUCT_BODY()

public:

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Tuning)
	FString Member;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Tuning)
	float MinMultiplier;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Tuning)
	float MaxMultiplier;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Tuning)
	float InitialValue;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Tuning)
	float InitialValueValid;

public:

	FTuningModifier() : MinMultiplier(1.f), MaxMultiplier(1.f), InitialValue(0.f), InitialValueValid(false){}

};

USTRUCT(Blueprintable, BlueprintType)
struct SUPERCARATTACK_API FTuningEntry
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Tuning)
	FName Name;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Tuning)
	float Value;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Tuning)
	TArray<FTuningModifier> Modifiers;

public:

	FTuningEntry() : Value(0.f)
	{

	}

	void Apply(AActor* Target);

};

UCLASS( Blueprintable, BlueprintType, ClassGroup=(VehicleTuning), meta=(BlueprintSpawnableComponent) )
class SUPERCARATTACK_API UTuningComponent : public UActorComponent
{
	GENERATED_BODY()

public:

	UPROPERTY(EditAnywhere, Category = Tuning)
	TArray<FTuningEntry> Entries;

public:

	UFUNCTION(BlueprintCallable, Category = Tuning)
	void AssignSettings(UObject* Settings);

	UFUNCTION(BlueprintCallable, Category = Tuning)
	void Apply();

public:	

	UTuningComponent();		
	
};
