#include "SuperCarAttack.h"
#include "SuperVehicle.h"
#include "TuningComponent.h"
#include "VehicleTuningData.h"
#include "WheeledVehicleMovementComponent.h"
#include "SuperPlayerController.h"


ASuperVehicle::ASuperVehicle()
{
	AutoPossessPlayer = EAutoReceiveInput::Player0;
	GetVehicleMovement()->bDeprecatedSpringOffsetMode = true;
}

void ASuperVehicle::BeginPlay()
{
	Super::BeginPlay();
	PlayerController = Cast<ASuperPlayerController>(GetWorld()->GetFirstPlayerController());

	if (TuningComponentClass)
	{
		VehicleTuningComponent = NewObject<UTuningComponent>(this, TuningComponentClass);

		if (VehicleTuningComponent)
		{
			ApplyTuning();
		}

	}


}

void ASuperVehicle::ApplyTuning()
{
	VehicleTuningComponent->AssignSettings(TuningData);
	VehicleTuningComponent->Apply();

}

void ASuperVehicle::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (!WaitRespawnCheck)
	{
		if (RespawnCheck())
		{
			WaitRespawnCheck = true;
			WaitRespawnCheckTime = 0;
		}
	}
	else
	{
		WaitRespawnCheckTime += DeltaTime;
		if (WaitRespawnCheckTime >= 2)
		{
			WaitRespawnCheck = false;
			if (RespawnCheck())
			{
				Destroy();
			}
		}
	}

}

void ASuperVehicle::Destroyed()
{
	Super::Destroyed();
	if (PlayerController)
	{
		PlayerController->RespawnVehicle();
	}

}

bool ASuperVehicle::RespawnCheck()
{
	bool Fallen = GetActorTransform().GetTranslation().Z < -50.0f;

	bool UpsideDown = FVector::DotProduct(GetActorUpVector(), FVector(0,0,1)) <= 0.25f;
	bool NotRolling = GetVehicleMovement()->GetForwardSpeed() <= 0.25f;
	
	return Fallen || (UpsideDown && NotRolling);
}




