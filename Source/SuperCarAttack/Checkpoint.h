#pragma once

#include "GameFramework/Actor.h"
#include "Checkpoint.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FCheckpointPassedDelegate, int, CheckpointIndex);

UCLASS(Blueprintable, BlueprintType)
class SUPERCARATTACK_API ACheckpoint : public AActor
{
	GENERATED_BODY()

public:

	UPROPERTY(Category = "Components", VisibleAnywhere, BlueprintReadOnly)
	class USceneComponent* Root;
		
	UPROPERTY(Category = "Components", VisibleAnywhere, BlueprintReadOnly)
	class UBoxComponent* Trigger;

	UPROPERTY(Category = "Components", VisibleAnywhere, BlueprintReadOnly)
	class UArrowComponent* Arrow;

	UPROPERTY(Category = "Components", EditAnywhere, BlueprintReadWrite)
	class UParticleSystemComponent* Particles;

	UPROPERTY(Category = "Values", EditAnywhere, BlueprintReadWrite)
	bool Respawn;

private:

	FCheckpointPassedDelegate CheckpointPassed;

	int CheckpointIndex;

public:

	ACheckpoint();

	virtual void Tick(float DeltaTime) override;

	void Activate();

	void Deactivate();

	FCheckpointPassedDelegate& GetCheckpointPassedDelegate();

	void SetCheckpointIndex(int CheckpointIndex);

	bool isRespawn();

protected:

	virtual void BeginPlay() override;

private:

	UFUNCTION(BlueprintCallable)
	void TriggerEndOverlap(class UPrimitiveComponent* HitComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);
	
};
