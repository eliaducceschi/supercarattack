#pragma once

#include "GameFramework/GameStateBase.h"
#include "SuperGameStateBase.generated.h"

UCLASS()
class SUPERCARATTACK_API ASuperGameStateBase : public AGameStateBase
{
	GENERATED_BODY()

public:

	static const float REFRESH_HUD_TIME;

	UPROPERTY(Category = "Race Settings", EditAnywhere, BlueprintReadWrite)
	int MaxLaps;

	UPROPERTY(Category = "Race Settings", EditAnywhere, BlueprintReadWrite)
	float GoldTime;

	UPROPERTY(Category = "Race Settings", EditAnywhere, BlueprintReadWrite)
	float SilverTime;

	UPROPERTY(Category = "Race Settings", EditAnywhere, BlueprintReadWrite)
	float BronzeTime;

	UPROPERTY(Category = "Race Settings", EditAnywhere, BlueprintReadWrite)
	float DefaultBestRaceTime;

	UPROPERTY(Category = "Race Settings", EditAnywhere, BlueprintReadWrite)
	float DefaultBestLapTime;

	UPROPERTY(Category = "Text", VisibleAnywhere, BlueprintReadOnly)
	FText MaxLapsText;

	UPROPERTY(Category = "Text", VisibleAnywhere, BlueprintReadOnly)
	FText GoldTimeText;

	UPROPERTY(Category = "Text", VisibleAnywhere, BlueprintReadOnly)
	FText SilverTimeText;

	UPROPERTY(Category = "Text", VisibleAnywhere, BlueprintReadOnly)
	FText BronzeTimeText;

	UPROPERTY(Category = "Text", VisibleAnywhere, BlueprintReadOnly)
	FText DefaultBestRaceTimeText;

	UPROPERTY(Category = "Text", VisibleAnywhere, BlueprintReadOnly)
	FText DefaultBestLapTimeText;

	UPROPERTY(Category = "Text", VisibleAnywhere, BlueprintReadOnly)
	FText BestRaceTimeText;

	UPROPERTY(Category = "Text", VisibleAnywhere, BlueprintReadOnly)
	FText BestLapTimeText;

private:

	float BestRaceTime;
	float BestLapTime;

	float TimerRefreshHUD;

public:

	void RefreshHUDInfo(float DeltaTime);
		
};
