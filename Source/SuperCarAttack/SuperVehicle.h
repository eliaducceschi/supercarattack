#pragma once

#include "SuperCarAttackPawn.h"
#include "SuperVehicle.generated.h"

UCLASS()
class SUPERCARATTACK_API ASuperVehicle : public ASuperCarAttackPawn
{
	GENERATED_BODY()

public:

	UPROPERTY(Category = "References", EditAnywhere, BlueprintReadWrite)
	TSubclassOf<class UTuningComponent> TuningComponentClass;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Component)
	class UTuningComponent* VehicleTuningComponent;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Tuning)
	class UVehicleTuningData* TuningData;

private:

	class ASuperPlayerController *PlayerController;

	bool WaitRespawnCheck = false;
	float WaitRespawnCheckTime = 0;

public:

	ASuperVehicle();

	virtual void Tick(float DeltaTime) override;

	void ApplyTuning();

protected:

	virtual void BeginPlay() override;
	virtual void Destroyed() override;

private:

	bool RespawnCheck();

};
