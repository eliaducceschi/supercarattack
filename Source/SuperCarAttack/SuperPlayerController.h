#pragma once

#include "GameFramework/PlayerController.h"
#include "SuperPlayerController.generated.h"

UCLASS(Blueprintable, BlueprintType)
class SUPERCARATTACK_API ASuperPlayerController : public APlayerController
{
	GENERATED_BODY()

public:

	UPROPERTY(Category = "References", EditAnywhere, BlueprintReadWrite)
	TSubclassOf<class UUserWidget> SuperHUDClass;

private:

	class UUserWidget* SuperHUD;

	class ASuperPlayerState *SuperPlayerState;
	class ASuperGameStateBase *SuperGameState;

	FTransform RespawnLocation;
	FVector OriginalScale3D;

	class ASuperVehicle *Vehicle;

	bool RaceCompleted;

	bool LapTimeStarted;
	bool RaceTimeStarted;

public:

	virtual void Tick(float DeltaTime) override;

	void Setup();
	void UpdateLap();
	void RespawnVehicle();
	bool RaceCompleteCheck();

protected:

	virtual void BeginPlay() override;

public:

	bool isRaceCompleted();

	void SetRespawnLocation(FTransform RespawnLocation);
	
};
