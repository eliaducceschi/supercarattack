#include "SuperCarAttack.h"
#include "Utilities.h"
#include "SuperGameStateBase.h"

const float ASuperGameStateBase::REFRESH_HUD_TIME = 1.0f;

void ASuperGameStateBase::RefreshHUDInfo(float DeltaTime)
{
	TimerRefreshHUD += DeltaTime;
	if (TimerRefreshHUD >= REFRESH_HUD_TIME)
	{
		MaxLapsText = FText::FromString(FString::FromInt(MaxLaps));

		BestRaceTimeText = UUtilities::TimeToText(BestRaceTime);
		BestLapTimeText = UUtilities::TimeToText(BestLapTime);

		GoldTimeText = UUtilities::TimeToText(GoldTime);
		SilverTimeText = UUtilities::TimeToText(SilverTime);
		BronzeTimeText = UUtilities::TimeToText(BronzeTime);
	}
}

