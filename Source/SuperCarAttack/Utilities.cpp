// Fill out your copyright notice in the Description page of Project Settings.

#include "SuperCarAttack.h"
#include "Utilities.h"

FText UUtilities::TimeToText(float Time)
{

	int Minutes = FGenericPlatformMath::FloorToInt(Time / 60);

	FString MinutesString = ZeroFormat(Minutes);

	int Seconds = FGenericPlatformMath::FloorToInt(Time) % 60;

	FString SecondsString = ZeroFormat(Seconds);

	int Milliseconds = FGenericPlatformMath::FloorToInt((Time - FGenericPlatformMath::FloorToInt(Time)) * 100);

	FString MillisecondsString = ZeroFormat(Milliseconds);

	FString TimeString(MinutesString+":"+SecondsString+":"+MillisecondsString);

	return FText::FromString(TimeString);

}

FString UUtilities::ZeroFormat(int ToFormat)
{
	FString ZeroFormatted("");

	if (ToFormat <= 9)
	{
		ZeroFormatted.Append("0");
	}	

	ZeroFormatted.AppendInt(ToFormat);

	return ZeroFormatted;
}




