#include "SuperCarAttack.h"
#include "SuperPlayerController.h"
#include "SuperGameStateBase.h"
#include "SuperGameModeBase.h"

void ASuperGameModeBase::BeginPlay()
{
	Super::BeginPlay();

	PlayerController = Cast<ASuperPlayerController>(GetWorld()->GetFirstPlayerController());
	if (PlayerController)
	{
		PlayerController->Setup();
	}

	SuperGameState = GetWorld()->GetGameState<ASuperGameStateBase>();

	SuperGameState->RefreshHUDInfo(ASuperGameStateBase::REFRESH_HUD_TIME);

}

void ASuperGameModeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	SuperGameState->RefreshHUDInfo(DeltaTime);

	RestartCheck(DeltaTime);
}

void ASuperGameModeBase::RestartCheck(float DeltaTime)
{
	if (PlayerController->isRaceCompleted())
	{
		TimerRestart += DeltaTime;
		if (TimerRestart >= 5)
		{
			PlayerController->RestartLevel();
		}
	}

}


