// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/HUD.h"
#include "SuperCarAttackHud.generated.h"


UCLASS(config = Game)
class ASuperCarAttackHud : public AHUD
{
	GENERATED_BODY()

public:
	ASuperCarAttackHud();

	/** Font used to render the vehicle info */
	UPROPERTY()
	UFont* HUDFont;

	// Begin AHUD interface
	virtual void DrawHUD() override;
	// End AHUD interface

};
