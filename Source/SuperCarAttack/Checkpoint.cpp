#include "SuperCarAttack.h"
#include "Checkpoint.h"
#include "SuperCarAttackPawn.h"
#include "Components/ArrowComponent.h"


ACheckpoint::ACheckpoint()
{

	Root = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	RootComponent = Root;

	Trigger = CreateDefaultSubobject<UBoxComponent>(TEXT("Trigger"));
	Trigger->SetupAttachment(RootComponent);
	Trigger->SetRelativeScale3D(FVector(2, 5, 5));
	Trigger->bGenerateOverlapEvents = true;
	Trigger->SetCollisionProfileName(FName("CheckpointOverlap"));
	Trigger->OnComponentEndOverlap.AddDynamic(this, &ACheckpoint::TriggerEndOverlap);

	Arrow = CreateDefaultSubobject<UArrowComponent>(TEXT("Arrow"));
	Arrow->SetupAttachment(RootComponent);

	Particles = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("Particles"));
	Particles->SetupAttachment(RootComponent);

	Respawn = true;

}

void ACheckpoint::BeginPlay()
{
	Super::BeginPlay();
	
}

void ACheckpoint::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ACheckpoint::Activate()
{
	Particles->SetHiddenInGame(false, false);
	Trigger->SetHiddenInGame(false, false);
	Trigger->bGenerateOverlapEvents = true;
}

void ACheckpoint::Deactivate()
{
	Particles->SetHiddenInGame(true, false);
	Trigger->SetHiddenInGame(true, false);
	Trigger->bGenerateOverlapEvents = false;
}

void ACheckpoint::TriggerEndOverlap(class UPrimitiveComponent* HitComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	ASuperCarAttackPawn* SuperCar = Cast<ASuperCarAttackPawn>(OtherActor);
	if (SuperCar)
	{
		FVector ForwardVector = Arrow->GetForwardVector();
		ForwardVector.Normalize();

		float DirectionCheck = FVector::DotProduct(SuperCar->GetVelocity(), ForwardVector);

		if (DirectionCheck > 0)
		{
			Deactivate();
			CheckpointPassed.Broadcast(CheckpointIndex);
		}

	}
}

FCheckpointPassedDelegate& ACheckpoint::GetCheckpointPassedDelegate()
{
	return CheckpointPassed;
}

void ACheckpoint::SetCheckpointIndex(int CheckpointIndex)
{
	this->CheckpointIndex = CheckpointIndex;
}

bool ACheckpoint::isRespawn()
{
	return Respawn;
}

