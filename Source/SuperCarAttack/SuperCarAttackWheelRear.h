// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "VehicleWheel.h"
#include "SuperCarAttackWheelRear.generated.h"

UCLASS()
class USuperCarAttackWheelRear : public UVehicleWheel
{
	GENERATED_BODY()

public:
	USuperCarAttackWheelRear();
};



