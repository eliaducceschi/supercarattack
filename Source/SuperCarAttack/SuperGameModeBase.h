#pragma once

#include "GameFramework/GameModeBase.h"
#include "SuperGameModeBase.generated.h"

UCLASS()
class SUPERCARATTACK_API ASuperGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

private:

	class ASuperGameStateBase *SuperGameState;

private:

	float TimerRestart = 0.0f;

	class ASuperPlayerController *PlayerController;

public:

	virtual void Tick(float DeltaTime) override;

protected:

	virtual void BeginPlay() override;

private:

	void RestartCheck(float DeltaTime);
	
};
