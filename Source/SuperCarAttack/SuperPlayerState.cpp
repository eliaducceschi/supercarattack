#include "SuperCarAttack.h"
#include "SuperPlayerState.h"
#include "Utilities.h"

const float ASuperPlayerState::REFRESH_HUD_TIME = 0.001f;

void ASuperPlayerState::RefreshHUDInfo(float DeltaTime)
{
	TimerRefreshHUD += DeltaTime;
	if (TimerRefreshHUD >= REFRESH_HUD_TIME)
	{
		TimerRefreshHUD = 0;

		CurrentLapText = FText::FromString(FString::FromInt(CurrentLap));
		CurrentRaceTimeText = UUtilities::TimeToText(CurrentRaceTime);
		CurrentLapTimeText = UUtilities::TimeToText(CurrentLapTime);

	}
}

void ASuperPlayerState::ResetState()
{
	CurrentLap = 1;
	CurrentLapTime = 0;
	CurrentRaceTime = 0;
}

void ASuperPlayerState::ResetLapTime()
{
	CurrentLapTime = 0;
}

void ASuperPlayerState::IncrementCurrentLap()
{
	++CurrentLap;
}

void ASuperPlayerState::UpdateCurrentLapTime(float DeltaTime)
{
	CurrentLapTime += DeltaTime;
}
void ASuperPlayerState::UpdateCurrentRaceTime(float DeltaTime)
{
	CurrentRaceTime += DeltaTime;
}

int ASuperPlayerState::GetCurrentLap()
{
	return CurrentLap;
}
