#pragma once

#include "GameFramework/PlayerState.h"
#include "SuperPlayerState.generated.h"

UCLASS()
class SUPERCARATTACK_API ASuperPlayerState : public APlayerState
{
	GENERATED_BODY()

public:

	static const float REFRESH_HUD_TIME;

	UPROPERTY(Category = "Text", VisibleAnywhere, BlueprintReadOnly)
	FText CurrentRaceTimeText;

	UPROPERTY(Category = "Text", VisibleAnywhere, BlueprintReadOnly)
	FText CurrentLapTimeText;

	UPROPERTY(Category = "Text", VisibleAnywhere, BlueprintReadOnly)
	FText CurrentLapText;

private:

	int CurrentLap;
	float CurrentRaceTime;
	float CurrentLapTime;

	float TimerRefreshHUD;

public:

	void RefreshHUDInfo(float DeltaTime);

	void ResetState();
	void ResetLapTime();

	void IncrementCurrentLap();
	void UpdateCurrentLapTime(float DeltaTime);
	void UpdateCurrentRaceTime(float DeltaTime);

	int GetCurrentLap();

};
