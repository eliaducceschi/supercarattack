#include "SuperCarAttack.h"
#include "SuperPlayerController.h"
#include "SuperVehicle.h"
#include "SuperPlayerState.h"
#include "SuperGameStateBase.h"
#include "Blueprint/UserWidget.h"
#include "Engine.h"

void ASuperPlayerController::BeginPlay()
{
	Super::BeginPlay();

	Vehicle = Cast<ASuperVehicle>(UGameplayStatics::GetPlayerPawn(GetWorld(),0));

	SuperGameState = GetWorld()->GetGameState<ASuperGameStateBase>();

	if (SuperHUDClass)
	{
		SuperHUD = CreateWidget<UUserWidget>(this, SuperHUDClass);

		if (SuperHUD)
		{
			SuperHUD->AddToViewport();
		}

	}

	LapTimeStarted = false;
	RaceTimeStarted = false;

}

void ASuperPlayerController::Setup()
{
	SuperPlayerState = Cast<ASuperPlayerState>(this->PlayerState);
	SuperPlayerState->ResetState();

	this->OriginalScale3D = Vehicle->GetActorTransform().GetScale3D();
	SetRespawnLocation(Vehicle->GetActorTransform());

	SuperPlayerState->RefreshHUDInfo(ASuperPlayerState::REFRESH_HUD_TIME);

	LapTimeStarted = true;
	RaceTimeStarted = true;
	RaceCompleted = false;
}

void ASuperPlayerController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (!RaceCompleted)
	{
		if (SuperPlayerState)
		{
			SuperPlayerState->RefreshHUDInfo(DeltaTime);

			if (LapTimeStarted)
			{
				SuperPlayerState->UpdateCurrentLapTime(DeltaTime);
			}
			if (RaceTimeStarted)
			{
				SuperPlayerState->UpdateCurrentRaceTime(DeltaTime);
			}
		}
	}
}

void ASuperPlayerController::UpdateLap()
{
	if (!RaceCompleted)
	{
		SuperPlayerState->IncrementCurrentLap();
		SuperPlayerState->RefreshHUDInfo(ASuperPlayerState::REFRESH_HUD_TIME);
		GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Blue, SuperPlayerState->CurrentLapText.ToString());
	}
	else
	{
		GEngine->AddOnScreenDebugMessage(-1, 3.0f, FColor::Blue, "WIN");
	}
}

void ASuperPlayerController::RespawnVehicle()
{
	FActorSpawnParameters SpawnInfo;
	ASuperVehicle *NewVehicle = GetWorld()->SpawnActor<ASuperVehicle>(ASuperVehicle::StaticClass(), RespawnLocation, SpawnInfo);
	Possess(NewVehicle);
}

bool ASuperPlayerController::RaceCompleteCheck()
{
	if (SuperPlayerState->GetCurrentLap() == SuperGameState->MaxLaps)
	{
		RaceCompleted = true;
	}
	else
	{
		RaceCompleted = false;
	}

	return RaceCompleted;
}

// Getter & Setter

bool ASuperPlayerController::isRaceCompleted()
{
	return RaceCompleted;
}

void ASuperPlayerController::SetRespawnLocation(FTransform RespawnLocation)
{
	this->RespawnLocation = RespawnLocation;
	this->RespawnLocation.SetScale3D(OriginalScale3D);
}