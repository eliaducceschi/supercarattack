#include "SuperCarAttack.h"
#include "RaceManager.h"
#include "Checkpoint.h"
#include "SuperPlayerController.h"
#include "Components/BillboardComponent.h"


ARaceManager::ARaceManager()
{
	Billboard = CreateDefaultSubobject<UBillboardComponent>(TEXT("Billboard"));
}

void ARaceManager::BeginPlay()
{
	Super::BeginPlay();
	PlayerController = Cast<ASuperPlayerController>(GetWorld()->GetFirstPlayerController());
	StartSequence();
	
}

void ARaceManager::StartSequence()
{
	for (int CheckpointIndex = 0; CheckpointIndex < Checkpoints.Num(); ++CheckpointIndex)
	{
		ACheckpoint *CheckpointToUpdate = Checkpoints[CheckpointIndex];
		CheckpointToUpdate->SetCheckpointIndex(CheckpointIndex);
		CheckpointToUpdate->Deactivate();
		CheckpointToUpdate->GetCheckpointPassedDelegate().AddDynamic(this, &ARaceManager::LapCheck);
	}

	NumCheckpoints = Checkpoints.Num();

	Checkpoints[0]->Activate();



}

void ARaceManager::LapCheck(int CheckpointPassedIndex)
{
	if (Checkpoints[CheckpointPassedIndex]->isRespawn())
	{
		PlayerController->SetRespawnLocation(Checkpoints[CheckpointPassedIndex]->GetActorTransform());
	}

	if (CheckpointPassedIndex == NumCheckpoints - 1)
	{
		if (!PlayerController->RaceCompleteCheck())
		{
			Checkpoints[0]->Activate();
			PlayerController->UpdateLap();
		}
	}
	else
	{
		Checkpoints[CheckpointPassedIndex + 1]->Activate();
	}

}