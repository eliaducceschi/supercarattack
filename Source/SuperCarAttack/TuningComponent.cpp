#include "SuperCarAttack.h"
#include "TuningComponent.h"

#include "SuperCarattackFunctionLibrary.h"

#pragma optimize("",off)

void FTuningEntry::Apply(AActor* Target)
{
	for (auto& Modifier : Modifiers)
	{
		UObject *OutParameter = nullptr;
		UProperty *Property = USuperCarAttackFunctionLibrary::RetrieveProperty(Target, Modifier.Member, OutParameter);

		if (Property != nullptr)
		{
			UFloatProperty *FloatProperty = Cast<UFloatProperty>(Property);
			UBoolProperty *BoolProperty = Cast<UBoolProperty>(Property);

			if (!Modifier.InitialValueValid)
			{
				if (FloatProperty != nullptr)
				{
					Modifier.InitialValue = FloatProperty->GetPropertyValue_InContainer(OutParameter);
				}
				else if (BoolProperty != nullptr)
				{
					Modifier.InitialValue = BoolProperty->GetPropertyValue_InContainer(OutParameter) ? 1.0f : 0.0f;
				}
				Modifier.InitialValueValid = true;
			}

			float NewValue = Modifier.InitialValue;
			float Multiplier = FMath::Lerp(Modifier.MinMultiplier, Modifier.MaxMultiplier, FMath::Clamp(Value,0.0f,1.0f));

			NewValue *= Multiplier;

			if (FloatProperty != nullptr)
			{
				FloatProperty->SetPropertyValue_InContainer(OutParameter, NewValue);
			}
			else if (BoolProperty != nullptr)
			{
				BoolProperty->SetPropertyValue_InContainer(OutParameter, NewValue > 0.5f);
			}
		}

	}
}

UTuningComponent::UTuningComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
}

void UTuningComponent::AssignSettings(UObject* Settings)
{
	if (Settings == nullptr)
	{
		return;
	}

	for (FTuningEntry& Entry : Entries)
	{
		UProperty* Property = Settings->GetClass()->FindPropertyByName(Entry.Name);
		if (Property != nullptr)
		{
			UFloatProperty* FloatProperty = Cast<UFloatProperty>(Property);
			if (FloatProperty != nullptr)
			{
				Entry.Value = FloatProperty->GetPropertyValue_InContainer(Settings);
			}
		}
	}
}

void UTuningComponent::Apply()
{
	for (auto& Entry : Entries)
	{
		Entry.Apply(GetOwner());
	}
}