// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "VehicleWheel.h"
#include "SuperCarAttackWheelFront.generated.h"

UCLASS()
class USuperCarAttackWheelFront : public UVehicleWheel
{
	GENERATED_BODY()

public:
	USuperCarAttackWheelFront();
};



