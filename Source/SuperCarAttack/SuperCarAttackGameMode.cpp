// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "SuperCarAttack.h"
#include "SuperCarAttackGameMode.h"
#include "SuperCarAttackPawn.h"
#include "SuperCarAttackHud.h"

ASuperCarAttackGameMode::ASuperCarAttackGameMode()
{
	DefaultPawnClass = ASuperCarAttackPawn::StaticClass();
	HUDClass = ASuperCarAttackHud::StaticClass();
}
